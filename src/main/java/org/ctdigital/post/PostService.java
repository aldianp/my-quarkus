package org.ctdigital.post;


import org.ctdigital.tag.Tag;
import org.ctdigital.tag.TagService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@ApplicationScoped
public class PostService {
    @Inject
    PostRepository postRepository;

    private final TagService tagService;

    @Inject
    public PostService(TagService tagService) {
        this.tagService = tagService;
    }

    public List<Post> getPosts(){
        return postRepository.listAll();
    }

    public Post getPostById(Long id){
        return postRepository.findById(id);
    }

    public void addNewPost(Post post){
        for (Long tagId : post.getTagIds()) {
            Optional<Tag> tag = tagService.getOptionalTagById(tagId);
            tag.ifPresent(post::addTag);
        }

        postRepository.persist(post);
    }

    public boolean deletePost(Long postId){
        return postRepository.deleteById(postId);
    }

    public void removeTag(Long postId, String name) {
        Tag tag1 = new Tag(name);
        Post post1 = postRepository.findById(postId);
        post1.removeTag(tag1);
    }

    public void updatePost(Long id, String title, String content){
        Post post = postRepository.findByIdOptional(id)
                .orElseThrow(() -> new IllegalStateException("post with id:"+id+" is not found"));
        if(title != null && title.length() > 0 && !Objects.equals(post.getTitle(), title)){
            post.setTitle(title);
        }
        if(content != null && content.length() > 0 && !Objects.equals(post.getContent(), content)){
            post.setContent(content);
        }
        postRepository.persist(post);
    }
}
