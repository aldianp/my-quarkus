package org.ctdigital.post;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@Path("/post")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PostController {


    private final PostService postService;

    @Inject
    public PostController(PostService postService) {
        this.postService = postService;
    }


    @GET
    public Response getAll() {
        List<Post> posts = postService.getPosts();
        return Response.ok(posts).build();
    }


    @POST
    @Transactional
    public void create(Post post) {
        postService.addNewPost(post);
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response deleteById(@PathParam("id") Long id) {
        boolean deleted = postService.deletePost(id);
        return deleted ? Response.noContent().build() : Response.status(BAD_REQUEST).build();
    }

    @DELETE
    @Path("{id}/{tagName}")
    @Transactional
    public void deleteTagName(@PathParam("id") Long id, @PathParam("tagName") String tagName) {
        postService.removeTag(id, tagName);
    }
}
