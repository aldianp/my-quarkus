package org.ctdigital.tag;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@Path("/tag")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TagController {

    private final TagService tagService;

    @Inject
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }


    @GET
    public Response getAll() {
        List<Tag> tags = tagService.getTags();
        return Response.ok(tags).build();
    }

    @GET
    @Path("{id}")
    public Response getById(@PathParam("id") Long id) {
        Tag tag = tagService.getTagById(id);
        return Response.ok(tag).build();
    }

    @POST
    @Transactional
    public void create(Tag tag) {
        tagService.addNewTag(tag);
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response deleteById(@PathParam("id") Long id) {
        boolean deleted = tagService.deleteTag(id);
        return deleted ? Response.noContent().build() : Response.status(BAD_REQUEST).build();
    }

    @PUT
    @Path("{id}/{name}")
    @Transactional
    public void updateTag(@PathParam("id") Long id, @PathParam("name") String name){
        tagService.updateTag(id, name);
    }

}
