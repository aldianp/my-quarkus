package org.ctdigital.tag;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@ApplicationScoped
public class TagService {
    @Inject
    TagRepository tagRepository;

    public List<Tag> getTags(){
        return tagRepository.listAll();
    }

    public Tag getTagById(Long id){
        return tagRepository.findById(id);
    }

    public Optional<Tag> getOptionalTagById(Long id){
        return tagRepository.findByIdOptional(id);
    }

    public void addNewTag(Tag tag){
        Optional<Tag> tagOptional = tagRepository.find("name", tag.getName())
                .singleResultOptional();
        if (tagOptional.isPresent()){
            throw new IllegalStateException("name taken");
        }
        tagRepository.persist(tag);
    }

    public boolean deleteTag(Long tagId){
        return tagRepository.deleteById(tagId);
    }

    public void updateTag(Long id, String name){
        Tag tag = tagRepository.findByIdOptional(id)
                .orElseThrow(() -> new IllegalStateException("tag with id:"+id+" is not found"));
        if(name != null && name.length() > 0 && !Objects.equals(tag.getName(), name)){
            tag.setName(name);
        }
        tagRepository.persist(tag);
    }
}
